package com.qamet.redisaop.aspect;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class RequestCounterAspect {

    private final RedissonClient redissonClient;
    public static final String KEY = "success_request_count";

    @AfterReturning(value = "execution(* com.qamet.redisaop.controller.AspectController.*()) && !execution(* com.qamet.redisaop.controller.AspectController.counts()) ", returning = "response")
    public void aspectRequest1(JoinPoint joinPoint, ResponseEntity<?> response) {

        if (!response.getStatusCode().is2xxSuccessful()) {
            return;
        }

        RMap<String, Integer> successRequestCountCache = redissonClient.getMapCache(KEY);
        String requestURI = getCurrentHttpRequest().get().getRequestURI();

        log.info("request endpoint : " + requestURI);

        if (successRequestCountCache.containsKey(requestURI)) {
            successRequestCountCache.put(requestURI, successRequestCountCache.get(requestURI) + 1);
        } else {
            successRequestCountCache.put(requestURI, 1);
        }

        log.info("{} method executed", joinPoint.getSignature().getName());
    }

    public static Optional<HttpServletRequest> getCurrentHttpRequest() {
        return Optional.ofNullable(RequestContextHolder.getRequestAttributes())
                .filter(ServletRequestAttributes.class::isInstance)
                .map(ServletRequestAttributes.class::cast)
                .map(ServletRequestAttributes::getRequest);
    }

}
