package com.qamet.redisaop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisAopApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisAopApplication.class, args);
	}

}
