package com.qamet.redisaop.controller;

import static com.qamet.redisaop.aspect.RequestCounterAspect.KEY;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/aop")
@RequiredArgsConstructor
@Slf4j
public class AspectController {

    private final RedissonClient redissonClient;
    Random random = new Random();

    @GetMapping("/request-1")
    public ResponseEntity<Integer> getData1() {
        int i = random.nextInt(1000);
        log.info("number : {}", i);

        if (i % 2 == 0) {
            log.info("success");
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            log.info("error");
            throw new RuntimeException();
        }
    }

    @GetMapping("/request-2")
    public ResponseEntity<Void> getData2() {
        int i = random.nextInt(1000);
        log.info("number : {}", i);

        if (i % 2 == 0) {
            log.info("success");
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            log.info("error");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/counts")
    public ResponseEntity<RMap<String, Integer>> counts() {
        return ResponseEntity.ok(redissonClient.getMapCache(KEY));
    }

}
